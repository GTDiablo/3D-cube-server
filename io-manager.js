class IOManager {
    callbacks = [];


    onLocalChange(callback){
        this.callbacks.push(callback);
    }

    nodeChange(node){
        console.log(node?.changes);
    }

    publishLocalChange(change){
        this.callbacks.forEach((cb)=> cb(change));
    }
}

module.exports = IOManager;